#!/usr/bin/env python

# Python script to convert Microsoft's .3mf files into usable .ply files
# written by flyingfences on 26 September 2015
# released under the X11 License

# .3mf must first be extracted (7-zip works), and this script run on the .model within
# written for Python 3.5, compatibilities with other versions not tested
# make sure to use an x64 install of Python, or you'll likely run out of memory


print("initializing")
from xml.dom import minidom

# load the .model extracted from the .3mf file into memory and get it all sorted out
print("parsing the file...", end='')
msf = minidom.parse('3dmodel.model')
print("done")
print("extracting: ", end='')
print("colors...", end='')
colors = msf.getElementsByTagName('color')
print("vertices...", end='')
verts = msf.getElementsByTagName('vertex')
print("triangles...", end='')
tris = msf.getElementsByTagName('triangle')
print("done")

# match colors to vertices
print("matching colors to vertices...", end='')
cols = []
for n in range(0,len(verts)):
	cols.append("#000000")
for n in range(0,len(tris)):
	cols[int(tris[n].attributes['v1'].value)] = colors[int(tris[n].attributes['colorid'].value.split(',')[0])].attributes['value'].value
	cols[int(tris[n].attributes['v2'].value)] = colors[int(tris[n].attributes['colorid'].value.split(',')[1])].attributes['value'].value
	cols[int(tris[n].attributes['v3'].value)] = colors[int(tris[n].attributes['colorid'].value.split(',')[2])].attributes['value'].value
# all that was incredibly redundant, but it was faster to write this way than to figure out how to only do each assignment once
print("done")

# write the header for the .ply file
print("writing file header...", end='')
ply = open('output.ply', 'w')
ply.write("ply\nformat ascii 1.0\n")
ply.write("element vertex %i\n" %len(verts))
ply.write("property float x\nproperty float y\nproperty float z\nproperty uchar red\nproperty uchar green\nproperty uchar blue\nproperty uchar alpha\n")
ply.write("element face %i\n" %len(tris))
ply.write("property list uchar int vertex_indices\nend_header\n")
print("done")

# writing the data to the output
# write the vertices first
# format: x y z r g b a
print("writing vertices...", end='')
for n in range(0,len(verts)):
	ply.write("%f %f %f %i %i %i 255\n" % (
	  float(verts[n].attributes['x'].value),
	  float(verts[n].attributes['y'].value),
	  float(verts[n].attributes['z'].value),
	  int(cols[n][1:3],16),
	  int(cols[n][3:5],16),
	  int(cols[n][5:7],16)))
print("done")
# then writing the faces
print("writing faces...", end='')
for n in range(0,len(tris)):
	ply.write("3 %i %i %i\n" % (
	  int(tris[n].attributes['v1'].value),
	  int(tris[n].attributes['v2'].value),
	  int(tris[n].attributes['v3'].value)))
print("done")

# close the output and terminate the program
ply.close()
print("conversion completed")
input("press return to exit")
